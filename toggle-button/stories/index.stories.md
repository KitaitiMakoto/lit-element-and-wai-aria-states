```js script
import { html } from '@open-wc/demoing-storybook';
import '../toggle-button.js';

export default {
  title: 'ToggleButton',
  component: 'toggle-button',
  options: { selectedPanel: "storybookjs/knobs/panel" },
};
```

# ToggleButton

A component for...

## Features:

- a
- b
- ...

## How to use

### Installation

```bash
yarn add toggle-button
```

```js
import 'toggle-button/toggle-button.js';
```

```js preview-story
export const Simple = () => html`
  <toggle-button></toggle-button>
`;
```

## Variations

###### Custom Title

```js preview-story
export const CustomTitle = () => html`
  <toggle-button title="Hello World"></toggle-button>
`;
```
