import { html, css, LitElement } from 'lit-element';

export class ToggleButton extends LitElement {
  static get styles() {
    return css`
      :host {
        padding: 0.5em 1em;
        background-color: #e8e8e8;
      }

      :host([aria-pressed="true"]) {
        background-color: #cccccc;
      }
    `;
  }

  static get properties() {
    return {
      pressed: {
        attribute: 'aria-pressed',
        reflect: true
      }
    };
  }

  render() {
    return html`
      <span @click=${this.toggle}><slot></slot></span>
    `;
  }

  connectedCallback() {
    super.connectedCallback();
    if (! this.hasAttribute('aria-pressed')) {
      this.pressed = false;
    } else {
      this.pressed = this.getAttribute('aria-pressed') === 'true';
    }
  }

  toggle(event) {
    this.pressed = ! this.pressed;
  }
}
